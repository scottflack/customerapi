﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using Bogus.DataSets;
using CustomerApi.Services.Models;

namespace CustomerApi.Services
{
    public class CustomerWebService : ICustomerWebService
    {
        private readonly List<Customer> _customers;

        public CustomerWebService()
        {
            var testUsers = new Faker<Customer>()
                .RuleFor(u => u.CustomerId, f => f.IndexFaker)
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FirstName, u.LastName))
                .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                .RuleFor(u => u.LastName, f => f.Name.LastName())
                .RuleFor(u => u.Username, f => f.Internet.UserName());

            _customers = testUsers.Generate(5000).ToList();
        }

        public async Task<Customer> GetCustomer(int customerId)
        {
            return _customers.FirstOrDefault(x => x.CustomerId == customerId);
        }

        public async Task<IEnumerable<Customer>> GetCustomers(int skip = 0, int take = 100)
        {
            return _customers.Skip(skip).Take(take);
        }

        public async Task<Customer> CreateOrUpdateCustomer(Customer customer)
        {
            var existingCustomer = _customers.FirstOrDefault(x => x.CustomerId == customer.CustomerId);

            if (existingCustomer == null)
            {
                _customers.Append(customer);
                existingCustomer = customer;
            }
            else
            {
                existingCustomer.Email = customer.Email;
                existingCustomer.FirstName = customer.FirstName;
                existingCustomer.LastName = customer.LastName;
                existingCustomer.Username = customer.Username;
            }

            return existingCustomer;
        }

        public async Task<bool> DeleteCustomer(int customerId)
        {
            var customer = _customers.First(x => x.CustomerId == customerId);

            if (customer == null)
                return false;

            return _customers.Remove(customer);
        }

    }
}
