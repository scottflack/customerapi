﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApi.Services.Models
{
    public class Order
    {
        public int OrderId { get; set; }

        public int CustomerId { get; set; }

        public Decimal TaxTotal { get; set; }

        public Decimal SubTotal { get; set; }

        public Decimal OrderTotal { get; set; }

        public string SalesRepresentative { get; set; }
    }
}
