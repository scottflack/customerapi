/* This is just to demonstrate a generic approach that allows it to work with any Model */
/* This repo provides a generic set of functionality for working with any type of model */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerApi.Services.Models;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi.Services
{
    public class ApplicationRepository<T> : IApplicationRepository<T> where T : class
    {
        private DbContext _context;

        public ApplicationRepository(DbContext context)
        {
            _context = context;
        }

        public void GetAll() { }

        public void GetById(Guid id) { }

        public void Create(T record) { }

        public void Update(T record) { }

        public void Delete(Guid id) { }

        public void Save() { }

        public void SaveAsync() { }

        public void Dispose(){}

    }
}