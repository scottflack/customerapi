﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerApi.Services.Models;

namespace CustomerApi.Services
{
    public interface IOrderWebService
    {
        Task<IEnumerable<Order>> GetOrdersByCustomer(int customerId);
    }
}