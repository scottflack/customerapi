/* This is just to demonstrate a generic approach that allows it to work with any Model */
/* This interface exposes a generic set of repository functions */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerApi.Services.Models;

namespace CustomerApi.Services
{
    public interface IApplicationRepository<T> 
    {
        void GetAll();

        void GetById(Guid id);

        void Create(T record);

        void Update(T record);

        void Delete(Guid id);

        void Save();

        void SaveAsync();
    }
}