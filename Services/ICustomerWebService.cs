﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerApi.Services.Models;

namespace CustomerApi.Services
{
    public interface ICustomerWebService
    {
        Task<Customer> GetCustomer(int customerId);
        Task<IEnumerable<Customer>> GetCustomers(int skip = 0, int take = 100);
        Task<Customer> CreateOrUpdateCustomer(Customer customer);
        Task<bool> DeleteCustomer(int customerId);
    }
}