﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using Bogus.DataSets;
using CustomerApi.Services.Models;

namespace CustomerApi.Services
{
    public class OrderWebService : IOrderWebService
    {
        private readonly List<Order> _orders;

        public OrderWebService()
        {
            var testOrders = new Faker<Order>()
                .RuleFor(o => o.OrderId, f => f.IndexFaker)
                .RuleFor(o => o.CustomerId, f => f.Random.Int(1, 5000))
                .RuleFor(o => o.SubTotal, f => Math.Round(f.Random.Decimal(0m, 1000m), 2))
                .RuleFor(o => o.TaxTotal, (f, o) => Math.Round(f.Random.Decimal(0, 0.1m) * o.SubTotal, 2))
                .RuleFor(o => o.OrderTotal, (f, o) => o.SubTotal + o.TaxTotal)
                .RuleFor(o => o.SalesRepresentative, f => f.Name.FullName());

            _orders = testOrders.Generate(100000).ToList();
        }

        public async Task<IEnumerable<Order>> GetOrdersByCustomer(int customerId)
        {
            return _orders.Where(x => x.CustomerId == customerId);
        }

    }
}
