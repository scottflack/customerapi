/* This is just to demonstrate a generic approach that allows it to work with any Model */
/* This controller allows for a set of common functionality to be inherited by other controllers */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using CustomerApi.Services;
using CustomerApi.Services.Models;
using Microsoft.AspNetCore.Mvc;

namespace CustomerApi.Controllers
{
    [Route("api/[controller]")]
    public abstract class BaseApiController<T> : Controller where T : class
    {

        [HttpGet("GetAll/{skip}/{take}")]
        public void Query(int skip = 0, int take = 100)
        {

        }

        [HttpGet("GetById/{id}")]
        public void Find(Guid id)
        {

        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] T record)
        {
            return NoContent();
        }

        [HttpPut("Update/{id}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] T record)
        {
            return NoContent();
        }

        [HttpDelete("Delete/{id}")]

        public async Task<IActionResult> Delete(Guid id)
        {
            return NoContent();
        }
    }
}