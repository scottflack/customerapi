﻿/* This is just to demonstrate a generic approach that allows it to work with any Model */
/* This controller inherits the base classes of another base controllers */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using CustomerApi.Services;
using CustomerApi.Services.Models;
using Microsoft.AspNetCore.Mvc;

namespace CustomerApi.Controllers
{
    [Route("api/orders")]
    public class OrderController : BaseApiController<Order>
    {

    }
}
