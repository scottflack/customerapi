﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using CustomerApi.Services;
using CustomerApi.Services.Models;
using Microsoft.AspNetCore.Mvc;

namespace CustomerApi.Controllers
{
    [Route("api/customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomerWebService _customerWebService;
        private readonly IOrderWebService _ordersWebService;

        public CustomerController(ICustomerWebService customerWebService, IOrderWebService ordersWebService)
        {
            _customerWebService = customerWebService;
            _ordersWebService = ordersWebService;
        }

        //This should ideally be implemented as generic functionality in a base class
        [HttpGet("GetCustomers/{skip}/{take}")]
        public async Task<IEnumerable<Customer>> GetCustomers(int skip = 0, int take = 100)
        {
            return await _customerWebService.GetCustomers(skip, take);
        }

        //This should ideally be implemented as generic functionality in a base class
        [HttpGet("GetCustomerById/{customerId}")]
        public async Task<Customer> GetCustomer(int customerId)
        {
            return await _customerWebService.GetCustomer(customerId);
        }

        [HttpGet("GetCustomerOrders/{customerId}")]
        public async Task<IEnumerable<Order>> GetOrders(int customerId)
        {
            return await _ordersWebService.GetOrdersByCustomer(customerId);
        }

        //This should ideally be implemented as generic functionality in a base class      
        [HttpPost("Update")]
        public async Task<Customer> Update(Customer c)
        {
            return await _customerWebService.CreateOrUpdateCustomer(c);
        }

        //This should ideally be implemented as generic functionality in a base class      
        [HttpDelete("Delete/{customerId}")]
        public async Task<bool> Delete(int customerId)
        {
            return await _customerWebService.DeleteCustomer(customerId);
        }
    }
}